﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace WeatherConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            string _urlLondon = "http://api.openweathermap.org/data/2.5/weather?q=London&units=metric&appid=065895ff10adf00cd1133b0b27772b72";
            HttpWebRequest _httpWebRequest = (HttpWebRequest)WebRequest.Create(_urlLondon);
            HttpWebResponse _httpWebResponse = (HttpWebResponse)_httpWebRequest.GetResponse();
            string response;
            using (StreamReader _streamReader = new StreamReader(_httpWebResponse.GetResponseStream()))
            {
                response = _streamReader.ReadToEnd();
            }
            WeatherResponse _weatherResponse = JsonConvert.DeserializeObject<WeatherResponse>(response);
            Console.WriteLine("Temperature in {0}: {1} °C", _weatherResponse.Name, _weatherResponse.Main.Temp);
            Console.ReadLine();
        }
    }
}
